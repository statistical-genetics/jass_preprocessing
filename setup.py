from setuptools import setup, find_packages

setup(name='jass_preprocessing',
      version='2.1',
      description='Preprocess and standardize heterogeneous GWAS summary statistic for JASS',
      url='http:https://gitlab.pasteur.fr/statistical-genetics/JASS_Pre-processing',
      author='Hugues Aschard, Hanna Julienne, Vincent Laville',
      author_email='hanna.julienne@pasteur.fr',
      license='MIT',
      #package_dir = {'': 'jass_preprocessing'},
      packages= ['jass_preprocessing'],
      zip_safe=False,
            install_requires=[
          'scipy', 'numpy', 'pandas', 'seaborn'
      ],
      entry_points={
          'console_scripts' : [
            'jass_preprocessing = jass_preprocessing.__main__:main'
          ]
      })

#, "jass_preprocessing.map_gwas","jass_preprocessing.dna_utils", "jass_preprocessing.map_reference","jass_preprocessing.compute_score", "jass_preprocessing.save_output"
