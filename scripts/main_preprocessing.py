"""
Read raw GWAS summary statistics, filter and format
Write clean GWAS datasets by chromosome
"""
__updated__ = '2018-26-06'

import numpy as np
import scipy.stats as ss
import sys
import math
import os
import pandas as pd
import matplotlib.pyplot as plt
import jass_preprocessing as jp
import pandas as pd
import seaborn as sns
import time


#Hard coded path (l.20-29 of JASS_Pre-processing/main_preprocessing.py)

#| variable name | description | current default value|
#|---------------|-------------|----------------------|
#| netPath | Main project folder, must end by "/" | /mnt/atlas/ |
#| GWAS_labels* | Path to the file describing the format of the individual GWASs files | netPath+'PCMA/1._DATA/RAW.GWAS/GWAS_labels.csv' |
#| GWAS_path* | Path to the folder containing the GWASs summ stat files, must end by "/" | netPath+'PCMA/1._DATA/RAW.GWAS/'|
#| diagnostic_folder | folder for histograms of sample size distribution among SNPs | /mnt/atlas/PCMA/1._DATA/sample_size_distribution/ |
#| ldscore_format | data formated to use LDscore, 1 file per study | /mnt/atlas/PCMA/1._DATA/ldscore_data/ |
#| REF_filename* | file containing the reference panel for imputation | netPath+'PCMA/0._REF/1KGENOME/summary_genome_Filter_part2.out' |
#| pathOUT | **unused in main_preprocessing.py**  | netPath+'PCMA/1._DATA/RAW.summary/'|
#| ImpG_output_Folder | main ouput folder | netPath+ 'PCMA/1._DATA/preprocessing_test/' |

#+ Hard coded variable: perSS = 0.7: the proportion of the 90th percentile of the sample size used to filter the SNPs



perSS = 0.7
netPath = "/mnt/atlas/"
GWAS_labels = netPath+'PCMA/1._DATA/RAW.GWAS/GWAS_labels.csv'
GWAS_path = netPath+'PCMA/1._DATA/RAW.GWAS/'

diagnostic_folder= "/mnt/atlas/PCMA/1._DATA/sample_size_distribution/"
ldscore_format="/mnt/atlas/PCMA/1._DATA/ldscore_data/"
REF_filename = netPath+'PCMA/0._REF/1KGENOME/summary_genome_Filter_part2.out'
pathOUT = netPath+'PCMA/1._DATA/RAW.summary/'

ImpG_output_Folder = netPath+ 'PCMA/1._DATA/preprocessing_test/'

# Read the information on GWAS file and set the gwas file name as index:

gwas_map = pd.read_csv(GWAS_labels, sep="\t", index_col=0)
#GWAS_table = gwas_map.index[22:]#["GIANT_2015_HIP_COMBINED_EUR.txt"]#"EUR.CD.gwas_filtered.assoc"]
#GWAS_table[5:]
# "GWAS_DBP_recoded.txt","GWAS_MAP_recoded.txt", #"GWAS_SBP_recoded_dummy.txt"
#              "GWAS_PP_recoded.txt","GWAS_SBP_recoded.txt",
#              "GIANT_2015_HIP_COMBINED_EUR.txt",
#              ]

GWAS_table = ["Menopause_HapMap_for_website_18112015.txt"] # gwas_map.index#["gabriel_asthma_dummy.txt"]

for GWAS_filename in GWAS_table:

    tag = "{0}_{1}".format(gwas_map.loc[GWAS_filename, 'consortia'],
                           gwas_map.loc[GWAS_filename, 'outcome'])
    print('processing GWAS: {}'.format(tag))
    start = time.time()
    gwas = jp.map_gwas.gwas_internal_link(GWAS_table, GWAS_path)

    GWAS_link = jp.map_gwas.walkfs(GWAS_path, GWAS_filename)[2]
    mapgw = jp.map_gwas.map_columns_position(GWAS_link, GWAS_labels)
    print(mapgw)

    gw_df = jp.map_gwas.read_gwas(GWAS_link, mapgw)

    ref = pd.read_csv(REF_filename, header=None, sep= "\t",
                      names =['chr', "pos", "snp_id", "ref", "alt", "MAF"],
                       index_col="snp_id")


    mgwas = jp.map_reference.map_on_ref_panel(gw_df, ref)

    mgwas = jp.map_reference.compute_snp_alignement(mgwas)
    mgwas = jp.compute_score.compute_z_score(mgwas)

    mgwas = jp.compute_score.compute_sample_size(mgwas, diagnostic_folder, tag)
    end = time.time()

    print("Preprocessing of {0} in {1}s".format(tag, end-start))

    jp.save_output.save_output_by_chromosome(mgwas, ImpG_output_Folder, tag)
    jp.save_output.save_output(mgwas, ldscore_format, tag)

mapgw.sort_values(inplace=True)
mgwas.head()

GWAS_labels
pd.read_csv(GWAS_labels, sep='\t', na_values='na', index_col=0)
