# Preprocessing package for cleaning and formating data for JASS

This helper package convert heterogeneous GWAS summary statistic format to
the JASS input format. Notably, this package realign GWAS summary statistic to a reference panel.


The **full package** documentation is available at http://statistical-genetics.pages.pasteur.fr/jass_preprocessing/