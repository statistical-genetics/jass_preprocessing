"""
    Few fonction to to compute DNA complement
"""


def dna_complement_base(inputbase):
    dna_complement_dict = {'A':'T', 'T':'A', 'G':'C', 'C':'G'}
    try:
        return(dna_complement_dict[inputbase])
    except KeyError:
        return('Not ATGC')


def dna_complement(input):
    return([dna_complement_base(x) for x in input])
