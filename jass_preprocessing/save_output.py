import pandas as pd



def save_output_by_chromosome(mgwas, ImpG_output_Folder, my_study):
    """
    Write the preprocessed Gwas for imputation
    """
    mgwas_copy = mgwas.reset_index(inplace=False)

    mgwas_copy.set_index("chr", inplace=True)
    mgwas_copy.dropna(subset=["computed_z"], how="any", inplace=True)
    print(mgwas_copy.index.unique())
    for chrom in mgwas_copy.index.unique():
        if type(mgwas_copy.loc[chrom]) is pd.core.frame.DataFrame:
            mgwas_chr = pd.DataFrame({
                            'rsID': mgwas_copy.loc[chrom].snp_id,
                            'pos': mgwas_copy.loc[chrom].pos,
                            'A0': mgwas_copy.loc[chrom].ref,
                            'A1':mgwas_copy.loc[chrom].alt,
                            'Z': mgwas_copy.loc[chrom].computed_z,
                            'P': mgwas_copy.loc[chrom].pval
                }, columns= ['rsID', 'pos', 'A0', "A1", "Z", "P" ])

            impg_output_file = ImpG_output_Folder + 'z_'+ my_study +'_chr'+str(chrom)+".txt"
            print("WRITING CHR {} results for {} to: {}".format(chrom, my_study, ImpG_output_Folder))
            mgwas_chr.sort_values(by="pos").to_csv(impg_output_file, sep="\t", index=False)
        else:
            print("NO or 1 value for CHR {} results for {} to: {}".format(chrom, my_study, ImpG_output_Folder))


def save_output(mgwas, ImpG_output_Folder, my_study):
    """
    Write the preprocessed Gwas for ldscore analysis
    """
    mgwas_copy = mgwas.reset_index(inplace=False)
    mgwas_copy.dropna(subset=["computed_z"], how="any", inplace=True)

    mgwas_copy = pd.DataFrame({
                    'chrom':mgwas_copy.chr,
                    'rsID': mgwas_copy.snp_id,
                    'pos': mgwas_copy.pos,
                    'A0': mgwas_copy.ref,
                    'A1':mgwas_copy.alt,
                    'Z': mgwas_copy.computed_z,
                    'P': mgwas_copy.pval,
                    "N": mgwas_copy.computed_N
        }, columns= ['chrom','rsID', 'pos', 'A0', "A1", "Z", "P", "N"])

    impg_output_file = ImpG_output_Folder + 'z_'+ my_study +".txt"
    print("WRITING results for {} to: {}".format( my_study, ImpG_output_Folder))
    print(mgwas_copy.head())
    mgwas_copy.sort_values(['chrom','pos']).to_csv(impg_output_file, sep="\t", index=False)
